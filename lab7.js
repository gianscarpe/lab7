
var express = require('express');
var app = express();

// Modulo per accedere al file system
var fs = require("fs");

// Modulo sparql e sparql endpoint
sparql = require("sparql")
var endpoint = 'https://dbpedia.org/sparql'; // Da modificare se si eseguono le query su dbpedia non italiano

// Modulo per fare richieste HTTP
var request = require('request');

// View engine
var pug = require('pug');

// Definisco dove si trovano le views e la loro estensione (.pug)
app.set('views', './views')
app.set('view engine', 'pug');

// Modulo per utilizzare i path, utilizzato per definire la cartella public
var path = require("path");

// Definisco la cartella "public" in root come accessibile dall'esterno
// serve a far leggere il file javascript di jquery
app.use("/public", 	express.static(path.join(__dirname, 'public')));

app.get('/airports', function (req, res)
{
	fs.readFile( __dirname + "/" + "airports.json", 'utf8', function (err, data)
	{
		var jsonData = JSON.parse(data);
		res.render('airportsList', { title: 'Lista aeroporti', message: 'Lista aeroporti', 'airportsList' : jsonData})
	});
})

app.get('/airport/:id', function (req, res)
{
	fs.readFile( __dirname + "/" + "airports.json", 'utf8', function (err, data)
	{
		var id = req.params.id
		var jsonData = JSON.parse(data);
		var abstr = "";
		currentAirport = jsonData[id]

		querySparql = "select distinct ?abstr where { ?airport dbo:iataLocationIdentifier '" + id + "' . ?airport dbo:abstract ?abstr filter(lang(?abstr) = 'en') .} LIMIT 1";
		client = new sparql.Client(endpoint)
		client.query(querySparql, function(error, result)
		{

			// Risultati in result.results.bindings[0].nome_variabile.value;
			abstr = result.results.bindings[0].abstr.value
			stringa = abstr.toString() + " (from DBpedia)"
			currentAirport.abstract = stringa
			res.render('airport', { title: currentAirport.name, message: currentAirport.name, 'currentAirport' : currentAirport})
		});

	})
});

app.get('/', function(req, res)
{
	// Redirect a home
	res.redirect('/home')
})

app.get('/home', function(req, res)
{
	// Mostro home, file pug già fatto
	// Se vuoi puoi abbellirlo come meglio credi
	res.render('home', { title: 'Home' });
})

app.get('/aboutUs', function (req, res)
{
	// Mostro pagina About us
	res.render('aboutUs', { title: 'About us' });
})

app.get('/flights', function(req, res)
{
		var array = []
		res.render("flights", { title: "Voli", 'airportsList' : array})
})

app.get("/searchResults", function(req, res)
{
	var data_ritorno = req.query.DateIn
	var data_partenza = req.query.DateOut
	var departure_code = req.query.Origin
	var destination_code = req.query.Destination

	var url = "http://sd2017.altervista.org/searchFlights.php?DateIn=" + data_ritorno +"&DateOut=" +data_partenza + "&Origin=" + departure_code + "&Destination=" + destination_code
	var results = new Array();

	request(url, function (error, response, body)
	{
		console.log('error:', error); // Print the error if one occurred
		console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
		console.log('body:', body); // Print the HTML
		res.end(body)
		// La gestione del json con i voli non è facile, se hai difficoltà mostra semplicemente il json che è stato ritornato.
	});
})

app.get('/routes', function(req, res)
{

	fs.readFile( __dirname + "/" + "airports.json", 'utf8', function (err, data)
	{
		var jsonData = JSON.parse(data);
		res.render('routes', { title: 'Rotte', message: 'Rotte', 'airportsList' : jsonData })
	});
})

app.get('/getDestination', function(req, res)
{
	var id = req.query.departure
	fs.readFile( __dirname + "/" + "routes.json", 'utf8', function (err, data)
	{
		var jsonData = JSON.parse(data);
		var airports = jsonData.airports
		var routes;
		for (var i = 0; i < airports.length; i++) {
			var current = airports[i]
			if (current.iataCode == id)
			{
				routes = current.routes;

			}

		}


		res.end(JSON.stringify(routes))

	});

	// Apri il file routes.json e cerca l'aeroporto di partenza, da li hai le informazioni su dove è possibile arrivare
});

/*
 * Apro il server su localhost (127.0.0.1) sulla porta 8081
 */
var server = app.listen(8081, '127.0.0.1', function () {
	var host = server.address().address;
	var port = server.address().port;
	console.log("Example app listening at http://%s:%s", host, port)
})


// Classi che possono aiutare nel rappresentare il Risultato con i Voli, potete aggiungere metodi se volete mostrare altre informazioni
// Potete anche cancellarle/ignorarle se non vi sono utili
/*
 * Result
 * Rappresenta un risultato, di base è composto da una data e da zero o più voli
 */
 /*
class Result {
	constructor()
	{
		this.flights = new Array();
	}

	setDate(date)
	{
		this.date = date;
	}

	addFlight(flight)
	{
		this.flights.push(flight);
	}
}
*/
/*
 * Flight
 * Rappresenta un volo, qui in versione ridotta con solo codice, ora di partenza e ora di arrivo.
 * Volendo si possono aggiungere posti rimanenti, costo e altre informazioni secondarie.
 *//*
class Flight
{
	setCode(code)
	{
		this.code = code;
	}

	setDeparture(departure)
	{
		this.departure = departure;
	}

	setArrival(arrival)
	{
		this.arrival = arrival;
	}

	setDepartureTime(departureTime)
	{
		this.departureTime = departureTime;
	}

	setArrivalTime(arrivalTime)
	{
		this.arrivalTime = arrivalTime;
	}
}*/
